from tarfile import ENCODING

import pandas as pd
import matplotlib
import os
from time import time
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn  import model_selection


#############classification models
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB

import re

import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.svm import SVC
from wordcloud import WordCloud,STOPWORDS
import matplotlib.pyplot as plt

read_ds = pd.read_csv("data_set/text_emotion.csv")
# Keeping only the neccessary columns
data = read_ds[['content','sentiment']]
################split the data_set to train and test ###################
df = pd.DataFrame(data)

train, test = train_test_split(df, test_size=0.65)
# split a training set and a test set
y_train = train.sentiment
y_test = test.sentiment
x_train= train.content
train_pos = train[train['sentiment'] == 'happiness']
train_pos = train_pos['content']
train_neg = train[train['sentiment'] == 'sadness']
train_neg = train_neg['content']


def wordcloud_draw(data, color='black'):
    words = ' '.join(data)
    cleaned_word = " ".join([word for word in words.split()
                             if 'http' not in word
                             and not word.startswith('@')
                             and not word.startswith('#')
                             and word != 'RT'
                             ])
    wordcloud = WordCloud(stopwords=STOPWORDS,
                          background_color=color,
                          width=2500,
                          height=2000
                          ).generate(cleaned_word)
    plt.figure(1, figsize=(13, 13))
    plt.imshow(wordcloud)
    plt.axis('off')
    plt.show()


print("Positive words")
wordcloud_draw(train_pos, 'white')
print("Negative words")
wordcloud_draw(train_neg)





########################REMOVING STOP WORDS#########################################

tweets = []
stopwords_set = set(stopwords.words("english"))

for index, row in train.iterrows():
    words_filtered = [e.lower() for e in row.content.split() if len(e) >= 3]
    words_cleaned = [word for word in words_filtered
        if 'http' not in word
        and not word.startswith('@')
        and not word.startswith('#')
        and word != 'RT']
    words_without_stopwords = [word for word in words_cleaned if not word in stopwords_set]
    tweets.append((words_without_stopwords, row.sentiment))
tweets_test=[];
for index, row in test.iterrows():
    words_filtered = [e.lower() for e in row.content.split() if len(e) >= 3]
    words_cleaned = [word for word in words_filtered
        if 'http' not in word
        and not word.startswith('@')
        and not word.startswith('#')
        and word != 'RT']
    words_without_stopwords = [word for word in words_cleaned if not word in stopwords_set]
    tweets_test.append((words_without_stopwords, row.sentiment))

test_pos = test[test['sentiment'] == 'happiness']
test_pos = test_pos['content']
test_neg = test[test['sentiment'] == 'sadness']
test_neg = test_neg['content']

############################# Extracting word features######################
def get_words_in_tweets(tweets):
    all = []
    for (words, sentiment) in tweets:
        all.extend(words)
    return all

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    features = wordlist.keys()
    return features
w_features = get_word_features(get_words_in_tweets(tweets))

w_features_test = get_word_features(get_words_in_tweets(tweets_test))

def extract_features(document):
    document_words = set(document)
    features = {}
    for word in w_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

def extract_features_test(document):
    document_words = set(document)
    features = {}
    for word in w_features_test:
        features['contains(%s)' % word] = (word in document_words)
    return features


wordcloud_draw(w_features)

# Training the Naive Bayes classifier
# Training the Naive Bayes classifier
training_set = nltk.classify.apply_features(extract_features,tweets)


# classifier = nltk.NaiveBayesClassifier.train(training_set)

test_set=nltk.classify.apply_features(extract_features_test,tweets_test)
print(training_set)
# classif = SklearnClassifier(BernoulliNB()).train(training_set)
#
# result_bernouli = classif.classify_many(test_set)
# accscore = metrics.accuracy_score(y_test, result_bernouli)
# print("bernouli results /n")
# print(result_bernouli)
# print(accscore)
# print("/n")
# classif = SklearnClassifier(SVC(), sparse=False).train(training_set)
#
# result_svc=classif.classify_many(test_set)
# accscore = metrics.accuracy_score(y_test, result_svc)
# print(accscore)
# print("SVC results /n")
# print(result_svc)



def estimation(x, y,X_test, estimator):
    estimator.fit(x, y)
    p = estimator.predict(X_test)
    accscore = metrics.accuracy_score(y_test, p)

    return accscore

estimators = [LinearSVC(loss='squared_hinge', penalty='l2', dual=False,max_iter=3000, tol=1e-3),
              KNeighborsClassifier(3),
              GradientBoostingClassifier(),

              KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
              metric_params=None, n_jobs=1, n_neighbors=5, p=2,
              weights='uniform'),
              SVC(kernel="linear", C=0.025),
              SVC(gamma=2, C=1),
              # GaussianProcessClassifier(1.0 * RBF(1.0)),
              # DecisionTreeClassifier(max_depth=5),
              RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
              MLPClassifier(alpha=1),
              AdaBoostClassifier(),
              # QuadraticDiscriminantAnalysis()
              ]

for est in estimators:
    print('\n', est, '\n')
    # accscore = estimation(X_train, y_train,X_test, est)
    accscore = estimation(training_set, y_train,test_set, est)

    print('accuracy score: %0.3f' % accscore)

















# REPLACE_NO_SPACE = re.compile("(\.)|(\;)|(\:)|(\!)|(\')|(\?)|(\,)|(\")|(\()|(\))|(\[)|(\])")
# REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")
#
#
# def preprocess_reviews(reviews):
#     reviews = [REPLACE_NO_SPACE.sub("", line.lower()) for line in reviews]
#     reviews = [REPLACE_WITH_SPACE.sub(" ", line) for line in reviews]
#
#     return reviews
#
#
# reviews_train_clean = preprocess_reviews(training_set.content)
# reviews_test_clean = preprocess_reviews(test.content)
#
#
#
# print("Extracting features from the training dataset using a sparse vectorizer")
# t0 = time()
#
# vectorizer = TfidfVectorizer(encoding=ENCODING, use_idf=True, norm='l2', binary=False, sublinear_tf=True, min_df=0.001,
#                              max_df=1.0, ngram_range=(1, 2), analyzer='word', stop_words=None)
#
# # the output of the fit_transform (x_train) is a sparse csc matrix.
# X_train = vectorizer.fit_transform(reviews_train_clean)
# duration = time() - t0
# print("done in %fs " % (duration))
# print("n_samples: %d, n_features: %d" % X_train.shape)
#
#
# print("Extracting features from the test dataset using the same vectorizer")
# t0 = time()
# X_test = vectorizer.transform(reviews_test_clean)
# duration = time() - t0
# print("test data_set features extracts in %fs" % (duration))
# print("n_samples: %d, n_features: %d" % X_test.shape)
# print()
#
# print("Extracting features from the unlabled dataset using the same vectorizer")
# read_unlabel_Df= pd.read_csv("data_set/un_labeled/unlabeled/unlabeled.csv")
# df= pd.DataFrame(read_unlabel_Df)
# t0 = time()
# X_unlabeled = vectorizer.transform(df.content)
# duration = time() - t0
# print("done in %fs" % (duration))
# print("n_samples: %d, n_features: %d" % X_unlabeled.shape)
# print()
#
#
#
#
# ##### evaluations
#
# def estimation(x, y,X_test, estimator):
#     estimator.fit(x, y)
#     p = estimator.predict(X_test)
#     accscore = metrics.accuracy_score(y_test, p)
#
#     return accscore
#
#
#
# estimators = [LinearSVC(loss='squared_hinge', penalty='l2', dual=False,max_iter=3000, tol=1e-3),
#               KNeighborsClassifier(3),
#               GradientBoostingClassifier(),
#
#               KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#               metric_params=None, n_jobs=1, n_neighbors=5, p=2,
#               weights='uniform'),
#               SVC(kernel="linear", C=0.025),
#               SVC(gamma=2, C=1),
#               # GaussianProcessClassifier(1.0 * RBF(1.0)),
#               # DecisionTreeClassifier(max_depth=5),
#               RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
#               MLPClassifier(alpha=1),
#               AdaBoostClassifier(),
#               # QuadraticDiscriminantAnalysis()
#               ]
#
# for est in estimators:
#     print('\n', est, '\n')
#     # accscore = estimation(X_train, y_train,X_test, est)
#     accscore = estimation(X_train_naive, y_train_naive,X_test_naive, est)
#
#     print('accuracy score: %0.3f' % accscore)


from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import make_classification
from active_learning.active_learning import ActiveLearner
import pandas as pd
import re
import  numpy as np
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn  import model_selection
from tarfile import ENCODING
read_ds = pd.read_csv("data_set/text_emotion.csv")
from time import time
################split the data_set to train and test ###################
################split the data_set to train and test ###################
df = pd.DataFrame(read_ds)
print(len(df))
df=df.loc[df['sentiment'].isin(['happiness','sadness','neutral'])]
print(len(df))
train, test = train_test_split(df, test_size=0.1)
# split a training set and a test set
y_train = train.sentiment
y_test = test.sentiment

REPLACE_NO_SPACE = re.compile("(\.)|(\;)|(\:)|(\!)|(\')|(\?)|(\,)|(\")|(\()|(\))|(\[)|(\])")
REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")


def preprocess_reviews(reviews):
    reviews = [REPLACE_NO_SPACE.sub("", line.lower()) for line in reviews]
    reviews = [REPLACE_WITH_SPACE.sub(" ", line) for line in reviews]

    return reviews


reviews_train_clean = preprocess_reviews(train.content)
reviews_test_clean = preprocess_reviews(test.content)



print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()

vectorizer = TfidfVectorizer(encoding=ENCODING, use_idf=True, norm='l2', binary=False, sublinear_tf=True, min_df=0.001,
                             max_df=1.0, ngram_range=(1, 2), analyzer='word', stop_words=None)

# the output of the fit_transform (x_train) is a sparse csc matrix.
X_train = vectorizer.fit_transform(reviews_train_clean)
duration = time() - t0
print("done in %fs " % (duration))
print("n_samples: %d, n_features: %d" % X_train.shape)


print("Extracting features from the test dataset using the same vectorizer")
t0 = time()
X_test = vectorizer.transform(reviews_test_clean)
duration = time() - t0
print("test data_set features extracts in %fs" % (duration))
print("n_samples: %d, n_features: %d" % X_test.shape)
print()

print("Extracting features from the unlabled dataset using the same vectorizer")
read_unlabel_Df= pd.read_csv("data_set/un_labeled/unlabeled/unlabeled.csv")
df= pd.DataFrame(read_unlabel_Df)
t0 = time()
X_unlabeled = vectorizer.transform(df.content)
duration = time() - t0
print("done in %fs" % (duration))
print("n_samples: %d, n_features: %d" % X_unlabeled.shape)
print()


X_labeled, X_unlabeled, y_labeled, y_oracle = train_test_split(
            X_train, y_train, test_size=0.75)


# X, X_unlabeled, y, y_oracle = train_test_split(*make_classification())
# print(X,y)
clf = LogisticRegression().fit(X_labeled, y_labeled)
# clf =LinearSVC(loss='squared_hinge', penalty='l2', dual=False,max_iter=3000, tol=1e-3).fit(X_train, y_train)
AL = ActiveLearner(strategy='max_margin')
result = AL.rank(clf, X_unlabeled, num_queries=10)
print(result)
print(X_unlabeled[result, :])
# print(sdf)
# print(len(sdf))
# sdf = sdf + X_unlabeled[result, :]
#
#
# # print(len(sdf))
# ### facing concatenation issue because of dynamic array size n dimensions
# X_augmented = np.concatenate((np.asarray(X_labeled), np.asarray(X_unlabeled[result, :])))#sparse.vstack((np.asarray(X_labeled),np.asarray(X_unlabeled[result, :]))) #
# y_augmented = np.concatenate((y_labeled, y_oracle[result]))
# clf=[LogisticRegression() for _ in range(10)]
#
# preds = []
# for model in clf:
#     model.fit(X_augmented, y_augmented)
#     preds.append(model.predict(X_test))
#
#
#
#
# print(preds)
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC, SVC
from modAL.models import ActiveLearner
from modAL.uncertainty import entropy_sampling
df= pd.read_csv("data_set/active_learning_basic.csv")

df=pd.DataFrame(df)


train, test = train_test_split(df, test_size=0.50)
X_train=train.drop(['label','Instances'], axis=1)
X_test=test.drop(['label','Instances'], axis=1)
y_train=train["label"]
y_test=test['label']
print(X_train)
clf=LinearSVC(loss='squared_hinge', penalty='l2', dual=False,max_iter=3000, tol=1e-3)
clf.fit(X_train,y_train)
y_hat=clf.predict(X_test)
train_idx = [1, 2,10 ]
# generating the pool
# X_pool = np.delete(df.drop(['Feature_A','Feature_B'], axis=1), train_idx, axis=0)
# y_pool = np.delete(df['label'], train_idx)
# initializing the active learner
learner = ActiveLearner(
    estimator=SVC(kernel='linear',probability=True),
    query_strategy=entropy_sampling,
    X_training=X_train, y_training=y_train
)


# pool-based sampling
n_queries = 5
for idx in range(n_queries):
    print(X_test.head(2))
    query_idx, query_instance = learner.query(np.asarray(X_test))
    print(query_instance,query_idx)

#     X_feature = X_test[query_idx].reshape(1, -1)
#     y_new_label = test[query_idx].reshape(1, )
#     print(X_feature,y_new_label)
#     learner.teach(
#         X=X_feature,
#         y=y_new_label
#     )



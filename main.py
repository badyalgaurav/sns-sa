import random
from tarfile import ENCODING

import pandas as pd
import matplotlib
import os
from time import time
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn  import model_selection
from sklearn.metrics import classification_report

#############classification models
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB

#########removing stop words import#####
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import re
import matplotlib.pyplot as plt









read_ds = pd.read_csv("data_set/text_emotion.csv")

################split the data_set to train and test ###################
df = pd.DataFrame(read_ds)
print(len(df))
df=df.loc[df['sentiment'].isin(['happiness','sadness','neutral'])]
print(len(df))
train, test = train_test_split(df, test_size=0.1)
# split a training set and a test set
y_train = train.sentiment
y_test = test.sentiment

REPLACE_NO_SPACE = re.compile("(\@)|(\.)|(\;)|(\:)|(\!)|(\')|(\?)|(\,)|(\")|(\()|(\))|(\[)|(\])")
REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")


def preprocess_reviews(reviews):
    stopWords = set(stopwords.words('english'))
    reviewsList=[];
    for line in reviews:
        words=word_tokenize(line)
        wordsFiltered=''
        for w in words:
            if w not in stopWords:
                wordsFiltered= wordsFiltered +' '+w;
        reviewsList.append(wordsFiltered);

    reviews = [ REPLACE_NO_SPACE.sub("", line.lower()) for line in reviewsList]
    reviews = [ REPLACE_NO_SPACE.sub("", line.lower()) for line in reviewsList]


    return reviews


reviews_train_clean = preprocess_reviews(train.content)
reviews_test_clean = preprocess_reviews(test.content)



print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()

vectorizer = TfidfVectorizer(encoding=ENCODING, use_idf=True, norm='l2', binary=False, sublinear_tf=True, min_df=0.001,
                             max_df=1.0, ngram_range=(1, 3), analyzer='word', stop_words=None)

# the output of the fit_transform (x_train) is a sparse csc matrix.
X_train = vectorizer.fit_transform(reviews_train_clean)
duration = time() - t0
print("done in %fs " % (duration))
print("n_samples: %d, n_features: %d" % X_train.shape)


print("Extracting features from the test dataset using the same vectorizer")
t0 = time()
X_test = vectorizer.transform(reviews_test_clean)
duration = time() - t0
print("test data_set features extracts in %fs" % (duration))
print("n_samples: %d, n_features: %d" % X_test.shape)
print()

print("Extracting features from the unlabled dataset using the same vectorizer")
read_unlabel_Df= pd.read_csv("data_set/un_labeled/unlabeled/unlabeled.csv")
df= pd.DataFrame(read_unlabel_Df)
t0 = time()
X_unlabeled = vectorizer.transform(df.content)
duration = time() - t0
print("done in %fs" % (duration))
print("n_samples: %d, n_features: %d" % X_unlabeled.shape)
print()




##### evaluations

def estimation(x, y,X_test, estimator):
    estimator.fit(x, y)
    y_hat = estimator.predict(X_test)
    accscore = metrics.accuracy_score(y_test, y_hat)
    report= classification_report(y_test,y_hat);

    return accscore,report,y_hat



estimators = [ LinearSVC(C=1.0, class_weight=None, dual=False, fit_intercept=True,
     intercept_scaling=1, loss='squared_hinge', max_iter=1000,
     multi_class='ovr', penalty='l2', random_state=None, tol=0.001,
     verbose=0) ,
               KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
                                    metric_params=None, n_jobs=1, n_neighbors=3, p=2,
                                    weights='uniform')
    ,
               GradientBoostingClassifier(criterion='friedman_mse', init=None,
                                          learning_rate=0.2, loss='deviance', max_depth=3,
                                          max_features=None, max_leaf_nodes=None,
                                          min_impurity_decrease=0.0, min_impurity_split=None,
                                          min_samples_leaf=1, min_samples_split=2,
                                          min_weight_fraction_leaf=0.0, n_estimators=100,
                                          presort='auto', random_state=None, subsample=1.0, verbose=0,
                                          warm_start=False),

              KNeighborsClassifier(algorithm='auto', leaf_size=10, metric='minkowski',
           metric_params=None, n_jobs=1, n_neighbors=24, p=2,
           weights='uniform'),
               SVC(C=0.025, cache_size=200, class_weight=None, coef0=0.0,
                   decision_function_shape='ovr', degree=3, gamma='auto', kernel='linear',
                   max_iter=-1, probability=False, random_state=None, shrinking=True,
                   tol=0.001, verbose=False),
              SVC(gamma=2, C=1),
              # GaussianProcessClassifier(1.0 * RBF(1.0)),
              # DecisionTreeClassifier(max_depth=5),
              RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=18, max_features=1, max_leaf_nodes=None,
            min_impurity_decrease=0.0, min_impurity_split=None,
            min_samples_leaf=1, min_samples_split=2,
            min_weight_fraction_leaf=0.0, n_estimators=10, n_jobs=1,
            oob_score=False, random_state=None, verbose=0,
            warm_start=False),
              MLPClassifier(activation='relu', alpha=1, batch_size='auto', beta_1=0.9,
                            beta_2=0.999, early_stopping=False, epsilon=1e-08,
                            hidden_layer_sizes=(200,), learning_rate='constant',
                            learning_rate_init=0.001, max_iter=200, momentum=0.9,
                            nesterovs_momentum=True, power_t=0.5, random_state=None,
                            shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
                            verbose=False, warm_start=False),
              AdaBoostClassifier(),
              # QuadraticDiscriminantAnalysis()
              ]
estimator_name=['LinearSVC','KNeighborsClassifier','GradientBoostingClassifier','SVC_kernal','SVC','SVC','RandomForestClassifier','MLPClassifier','AdaBoostClassifier']
estimator_Accuracy=[];
y_actual_Df = pd.DataFrame(np.array(y_test), columns=['sentiments']);
real_extracted_sentiments_happiness_count = len(y_actual_Df.loc[y_actual_Df['sentiments'] == 'happiness'])
real_extracted_sentiments_neutral_count =len( y_actual_Df.loc[y_actual_Df['sentiments'] == 'neutral'])
real_extracted_sentiments_sadness_count = len(y_actual_Df.loc[y_actual_Df['sentiments'] == 'sadness'])

for i,est in enumerate(estimators):
    print('\n', est, '\n')
    accscore,report,y_hat = estimation(X_train, y_train,X_test, est)

    y_hata_Df= pd.DataFrame(y_hat,columns=['sentiments']);
    predicted_extracted_sentiments_happiness_count=len(y_hata_Df.loc[y_hata_Df['sentiments'] == 'happiness'])
    predicted_extracted_sentiments_neutral_count=len(y_hata_Df.loc[y_hata_Df['sentiments'] == 'neutral'])
    predicted_extracted_sentiments_sadness_count=len(y_hata_Df.loc[y_hata_Df['sentiments'] == 'sadness'])

    ##########################Graphical Results
    bar_width = 0.35
    index = np.arange(3)
    fig, ax = plt.subplots()

    # Generate dummy data into a dataframe
    pred_happiness_Bar = ax.bar(index, y_hata_Df["sentiments"].value_counts(), bar_width,
                    label="pred happiness")

    actual_happiness_Bar = ax.bar(index + bar_width, y_actual_Df['sentiments'].value_counts(),
                    bar_width, label="actual happiness")

    ax.set_xlabel('Sentiments')
    ax.set_ylabel('predicted vs actual')
    ax.set_title(estimator_name[i])
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(["Happiness", "Neutral", "Sadness"])
    ax.legend()
    plt.savefig('data_set/results/'+estimator_name[i]+'.png')

    plt.show()
















    # encoded_series = y_hat.apply(le.fit_transform)
    estimator_Accuracy.append(accscore*100);
    print('accuracy score: %0.3f' % accscore)
    print('Classfication report',report)






##########################Graphical Results
fig, ax = plt.subplots()
objects = ('LinearSVC','KNeighborsClassifier','GradientBoostingClassifier','KNeighborsClassifier','SVC_kernal','SVC','RandomForestClassifier','MLPClassifier','AdaBoostClassifier')
y_pos = np.arange(len(objects))
performance = [10, 8, 6, 4, 2, 1]

plt.barh(y_pos, estimator_Accuracy, align='center', alpha=0.5)
plt.yticks(y_pos, objects)
plt.xlabel('Accuracy in %')
plt.title('Classifier Comparison')
 #Change of fontsize and angle of xticklabels
plt.setp(ax.get_xticklabels(), fontsize=10, rotation='vertical')
plt.autoscale(True)
plt.show()
# plt.bar(x, money)
# plt.xticks(estimator_Accuracy, estimator_name)
# plt.show()

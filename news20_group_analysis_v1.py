import pandas as pd
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier, AdaBoostClassifier
from sklearn.feature_extraction.text import CountVectorizer

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

from subprocess import check_output

from sklearn.metrics import classification_report
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC, SVC

print(check_output(["ls", "data_set/"]).decode("utf8"));


#Using sentiment analysis dictionary as training data set

train_ds = pd.read_csv( "data_set/dict.csv",
                       delimiter=",", encoding = "ISO-8859-1",
                       names = ["Text", "Sentiment"] )


#Replacing positive with 1 and negative with 0
train_ds['Sentiment'] = train_ds['Sentiment'].map({'positive': 1, 'negative': 0})



count_vectorizer = CountVectorizer( max_features = 5000 )


feature_vector = count_vectorizer.fit( train_ds.Text )
train_ds_features = count_vectorizer.transform( train_ds.Text )

features = feature_vector.get_feature_names()

# print(features[0:20])


features_counts = np.sum( train_ds_features.toarray(), axis = 0 )


feature_counts = pd.DataFrame( dict( features = features,
                                  counts = features_counts ) )

# print(feature_counts.head(5))

#Removing stop words



count_vectorizer = CountVectorizer( stop_words = "english", max_features = 5000 )
feature_vector = count_vectorizer.fit(train_ds.Text )
train_ds_features = count_vectorizer.transform(train_ds.Text)

# print(train_ds_features)


features = feature_vector.get_feature_names()
features_counts = np.sum( train_ds_features.toarray(), axis = 0 )
feature_counts = pd.DataFrame( dict( features = features,
                                  counts = features_counts ) )
feature_counts.sort_values( "counts", ascending = False )[0:20]


# print(feature_counts)


#########Loading test dataset############################

test_ds =  pd.read_csv("data_set/abcnews.csv",delimiter=",", encoding = "utf-8" )

df_a= test_ds['publish_date'];


print(test_ds.count())

test_ds.dropna()

print(test_ds.count())
# print(test_ds['headline_text'])
headline_text = count_vectorizer.transform(test_ds['headline_text'])
# print(headline_text)
headline_text = count_vectorizer.transform(test_ds['headline_text'].astype('U'))

print("test",headline_text[1])

#########classification section
from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import train_test_split
clf = GaussianNB()

from nltk.corpus import stopwords





train_x, test_x, train_y, test_y = train_test_split( train_ds_features,
                                                  train_ds.Sentiment,
                                                  test_size = 0.3,
                                                  random_state = 42 )


clf.fit( train_ds_features.toarray(), train_ds.Sentiment )
test_x_test_ds = count_vectorizer.transform(test_ds['headline_text'].head(100))



test_ds_predicted = clf.predict( test_x_test_ds.toarray() )

from sklearn import metrics
cm = metrics.confusion_matrix( test_y, test_ds_predicted )

# print(cm)


score = metrics.accuracy_score( test_y, test_ds_predicted )
test_ds["sentiment"] = clf.predict( headline_text.toarray() )

# print(test_ds["sentiment"])
print(score)


def estimation(x, y,X_test, estimator):
    estimator.fit(train_x.toarray(), train_y)
    p = estimator.predict( test_x.toarray())
    accscore = metrics.accuracy_score(test_y, p)
    report= classification_report(test_y,p);

    return accscore,report



estimators = [LinearSVC(loss='squared_hinge', penalty='l2', dual=False,max_iter=3000, tol=1e-3),
              KNeighborsClassifier(3),
              GradientBoostingClassifier(),

              KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
              metric_params=None, n_jobs=1, n_neighbors=5, p=2,
              weights='uniform'),
              SVC(kernel="linear", C=0.025),
              SVC(gamma=2, C=1),
              # GaussianProcessClassifier(1.0 * RBF(1.0)),
              # DecisionTreeClassifier(max_depth=5),
              RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
              MLPClassifier(alpha=1),
              AdaBoostClassifier(),
              # QuadraticDiscriminantAnalysis()
              ]
estimator_name=['LinearSVC','KNeighborsClassifier','GradientBoostingClassifier','SVC_kernal','SVC','RandomForestClassifier','MLPClassifier','AdaBoostClassifier']
estimator_Accuracy=[];
for est in estimators:
    print('\n', est, '\n')
    print(train_x.toarray())
    accscore,report = estimation(train_x.toarray(), train_y ,headline_text.toarray() , est)
    estimator_Accuracy.append(accscore*100);
    print('accuracy score: %0.3f' % accscore)
    print('Classfication report',report)
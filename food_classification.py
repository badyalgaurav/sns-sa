import numpy as np
import pandas as pd
from tarfile import ENCODING

from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cross_validation import train_test_split

from sklearn.svm import SVC

#############classification models
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
food_file_path='data_set/generic-food.csv'

df= pd.read_csv(food_file_path);





X= df['FOODNAME']
Y=df['GROUP']

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, random_state=42)
vectorizer = TfidfVectorizer(encoding=ENCODING, use_idf=True, norm='l2', binary=False, sublinear_tf=True, min_df=0.001,
                             max_df=1.0, ngram_range=(1, 3), analyzer='word', stop_words=None)
X_train_dtm=vectorizer.fit_transform(X_train)
X_test = vectorizer.transform(X_test)

def estimation(x, y,X_test, estimator):
    estimator.fit(x, y)
    p = estimator.predict(X_test)
    accscore = metrics.accuracy_score(y_test, p)
    # report= classification_report(y_test,p);

    return accscore



estimators = [
               KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
                                    metric_params=None, n_jobs=1, n_neighbors=3, p=2,
                                    weights='uniform')
    ,
               GradientBoostingClassifier(criterion='friedman_mse', init=None,
                                          learning_rate=0.2, loss='deviance', max_depth=3,
                                          max_features=None, max_leaf_nodes=None,
                                          min_impurity_decrease=0.0, min_impurity_split=None,
                                          min_samples_leaf=1, min_samples_split=2,
                                          min_weight_fraction_leaf=0.0, n_estimators=100,
                                          presort='auto', random_state=None, subsample=1.0, verbose=0,
                                          warm_start=False),

              KNeighborsClassifier(algorithm='auto', leaf_size=10, metric='minkowski',
           metric_params=None, n_jobs=1, n_neighbors=24, p=2,
           weights='uniform'),
               SVC(C=0.025, cache_size=200, class_weight=None, coef0=0.0,
                   decision_function_shape='ovr', degree=3, gamma='auto', kernel='linear',
                   max_iter=-1, probability=False, random_state=None, shrinking=True,
                   tol=0.001, verbose=False),
              SVC(gamma=2, C=1),
              # GaussianProcessClassifier(1.0 * RBF(1.0)),
              # DecisionTreeClassifier(max_depth=5),
              RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=18, max_features=1, max_leaf_nodes=None,
            min_impurity_decrease=0.0, min_impurity_split=None,
            min_samples_leaf=1, min_samples_split=2,
            min_weight_fraction_leaf=0.0, n_estimators=10, n_jobs=1,
            oob_score=False, random_state=None, verbose=0,
            warm_start=False),
              MLPClassifier(activation='relu', alpha=1, batch_size='auto', beta_1=0.9,
                            beta_2=0.999, early_stopping=False, epsilon=1e-08,
                            hidden_layer_sizes=(200,), learning_rate='constant',
                            learning_rate_init=0.001, max_iter=200, momentum=0.9,
                            nesterovs_momentum=True, power_t=0.5, random_state=None,
                            shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
                            verbose=False, warm_start=False),
              AdaBoostClassifier(),
              # QuadraticDiscriminantAnalysis()
              ]
estimator_name=['LinearSVC','KNeighborsClassifier','GradientBoostingClassifier','SVC_kernal','SVC','RandomForestClassifier','MLPClassifier','AdaBoostClassifier']
estimator_Accuracy=[];
for est in estimators:
    print('\n', est, '\n')
    accscore = estimation(X_train_dtm, y_train,X_test, est)
    estimator_Accuracy.append(accscore*100);
    print('accuracy score: %0.3f' % accscore)
    # print('Classfication report',report)


print(accscore)